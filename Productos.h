//
//  Productos.h
//  elektrapp
//
//  Created by Rafael Castañeda on 10/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Productos : NSObject{

NSInteger idProducto;
 NSInteger llaveProducto;
 NSString *describeProducto;
}

@property (nonatomic, assign) NSInteger idProducto;
@property (nonatomic, assign)NSInteger llaveProducto;
@property (nonatomic, retain)NSString *describeProductos;

@end
