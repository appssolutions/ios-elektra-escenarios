//
//  ViewController.h
//  elektrapp
//
//  Created by Rafael Castañeda on 08/08/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *seleccionRecamara;
@property (strong, nonatomic) IBOutlet UIButton *seleccionComedor;
@property (strong, nonatomic) IBOutlet UIButton *seleccionCocina;
@property (strong, nonatomic) IBOutlet UIButton *seleccionSala;

@property (strong, nonatomic) IBOutlet UIView *despliegaEscenarios;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UILabel *vista;
@property (strong, nonatomic) IBOutlet UIView *vistaEscenario;
@property (strong, nonatomic) IBOutlet UIButton *miSeleccion;

- (IBAction)seleccionEscenario:(UIButton *)sender;



- (void)desplegandoEscenario: (int) secciones : (int)comienzaEn;



@end
