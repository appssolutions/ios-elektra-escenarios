//
//  ProductosDAO.m
//  elektrapp
//
//  Created by Rafael Castañeda on 11/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import "ProductosDAO.h"
#import "Productos.h"


@implementation ProductosDAO



- (NSString *) obtenerRutaBD{
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"elektraAppes.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD] == NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"elektraAppes.sqlite"] toPath:rutaBD error:NULL];
    }
    
    return rutaBD;
}


- (NSMutableArray *)obtenerArbolProductos{

    NSMutableArray *listaRamas = [[NSMutableArray alloc] init];
	NSString *ubicacionDB = [self obtenerRutaBD];
	
	if(!(sqlite3_open([ubicacionDB UTF8String], &bd) == SQLITE_OK)){
		NSLog(@"No se puede conectar con la BD");
	}
	
	const char *sentenciaSQL = "SELECT * FROM arbolProductos";
	sqlite3_stmt *sqlStatement;
	
	if(sqlite3_prepare_v2(bd, sentenciaSQL, -1, &sqlStatement, NULL) != SQLITE_OK){
		NSLog(@"Problema al preparar el statement");
        
        NSLog(@"%d",sqlite3_prepare_v2(bd, sentenciaSQL, -1, &sqlStatement, NULL));
	}
	
	while(sqlite3_step(sqlStatement) == SQLITE_ROW){
		Productos *ramificacion = [[Productos alloc] init];
		ramificacion.idProducto = sqlite3_column_int(sqlStatement, 0);
        ramificacion.llaveProducto = sqlite3_column_int(sqlStatement, 1);
		ramificacion.describeProductos = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 2)];
		
		
		[listaRamas addObject:ramificacion];
	}
	
	return listaRamas;



}


@end
