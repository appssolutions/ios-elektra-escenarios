//
//  ProductosDAO.h
//  elektrapp
//
//  Created by Rafael Castañeda on 11/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ProductosDAO : NSObject{

    sqlite3 *bd;

}

- (NSMutableArray *)obtenerArbolProductos;


@end
