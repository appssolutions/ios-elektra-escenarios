//
//  ProjectMilestoneController.m
//  elektrapp
//
//  Created by Rafael Castañeda on 03/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import "ProjectMilestoneController.h"
#import "TableViewControllerCategorias.h"
#import "AppDelegate.h"

@interface ProjectMilestoneController ()

@end

@implementation ProjectMilestoneController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
       
    [self muestraTabla];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) muestraTabla{

    categorias = [[NSMutableArray alloc] init];
    [categorias addObject:@"Refrigeradores"];
    [categorias addObject:@"Estufas"];
    [categorias addObject:@"Microondas"];
    [self setTitle:@"Cocina"];


}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [categorias count];
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    
    // Configure the cell...
    
    cell.textLabel.text = [categorias objectAtIndex:indexPath.row];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewControllerCategorias *categori = [[TableViewControllerCategorias alloc] init];
    
    
  
    
    if ([[categorias objectAtIndex:indexPath.row]isEqual:@"Refrigeradores"]) {
        categori.categoriaInt = 0;
        [categori setTitle:[categorias objectAtIndex:indexPath.row]];
    }
    
    if ([[categorias objectAtIndex:indexPath.row]isEqual:@"Estufas"]) {
        categori.categoriaInt = 1;
        [categori setTitle:[categorias objectAtIndex:indexPath.row]];
    }
    
    if ([[categorias objectAtIndex:indexPath.row]isEqual:@"Microondas"]) {
        categori.categoriaInt = 2;
        [categori setTitle:[categorias objectAtIndex:indexPath.row]];
    }
    
   [self performSegueWithIdentifier:@"segue1" sender:self];     
}



@end
