//
//  TableViewControllerCategorias.h
//  elektrapp
//
//  Created by Rafael Castañeda on 04/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewControllerCategorias : UITableViewController{

    NSMutableArray *refrigeradores;
    NSMutableArray *estufas;
    NSMutableArray *hornos;
    
    int categoriaInt;


}
@property int categoriaInt;
@end
