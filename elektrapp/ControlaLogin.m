//
//  ControlaLogin.m
//  elektrapp
//
//  Created by Rafael Castañeda on 03/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import "ControlaLogin.h"

@interface ControlaLogin ()

@end

@implementation ControlaLogin
@synthesize bienvenido;

 NSArray *tableData;
NSArray *tableNombres;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tableData = [NSArray arrayWithObjects:@"56786544", @"43456754", @"57865456", @"62234569", @"67834567", @"65432356", @"64788907", @"76456654", @"11222345", @"57654354", @"65554334", @"65544354", nil];
    
    
    tableNombres = [NSArray arrayWithObjects:@"Rafael Castañeda", @"Norberto Maldonado", @"Ruben Calixto ", @"Manuel Fernandez", @"Francisco Gutierrez", @"Alberto Corona", @"Violeta Pacheco", @"Daniel Moreno", @"Roberto Sosa", @"Manuel Fernandez", @"Daniel Romero", @"Humberto Suarez", nil];

	// Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // self.value = indexPath.row;
    
    NSLog(@"%d",indexPath.row);
    bienvenido.text = [NSString stringWithFormat:@"Bienvenido %@",[tableNombres objectAtIndex:indexPath.row]];
    
    //UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:nowIndex];
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
