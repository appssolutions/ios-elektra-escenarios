//
//  ControlaLogin.h
//  elektrapp
//
//  Created by Rafael Castañeda on 03/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ControlaLogin : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UILabel *bienvenido;

@end
