//
//  AppDelegate.h
//  tablaProductos
//
//  Created by Rafael Castañeda on 05/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
