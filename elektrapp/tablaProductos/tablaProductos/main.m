//
//  main.m
//  tablaProductos
//
//  Created by Rafael Castañeda on 05/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
