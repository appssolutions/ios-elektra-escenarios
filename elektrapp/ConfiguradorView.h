//
//  ConfiguradorView.h
//  elektrapp
//
//  Created by Rafael Castañeda on 14/08/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "primerConsulta.h"
#import "segundaConsulta.h"
#import "ProductosDAO.h"


@interface ConfiguradorView : UIViewController <UIGestureRecognizerDelegate,UITableViewDataSource, UITableViewDelegate>
{
    
    primerConsulta *firstController;
	segundaConsulta *secondController;
	IBOutlet UITableView *firstTable;
	IBOutlet UITableView *secondTable;
    
NSInteger selectedButton;
    
    ProductosDAO *listaRamas;
    NSMutableArray *cargaRamaActual;
    
}

@property (strong, nonatomic) ProductosDAO *listaRamas;
@property (strong, nonatomic) NSMutableArray *cargaRamaActual;



@property (strong, nonatomic) IBOutlet UIView *vistaMenuTop;
@property (strong, nonatomic) IBOutlet UIView *vistaMenuDown;
@property (strong, nonatomic) IBOutlet UIButton *boton;
@property (strong, nonatomic) IBOutlet UIView *escenarioVistas;
@property (strong, nonatomic) IBOutlet UIImageView *mueveImagen;
@property (strong, nonatomic) IBOutlet UIImageView *imagenCentro;
@property (strong, nonatomic) IBOutlet UIImageView *imagenIzquierda;
@property (strong, nonatomic) IBOutlet UIImageView *imagenDerecha;
@property (strong, nonatomic) IBOutlet UIView *posiciones;
@property (strong, nonatomic) IBOutlet UIView *colores;
@property (strong, nonatomic) IBOutlet UIButton *camara;
@property (strong, nonatomic) IBOutlet UIButton *rodillo;
@property (strong, nonatomic) IBOutlet UIImageView *mueveFondo;

@property (strong, nonatomic) IBOutlet UIImageView *mueveDerecha;
@property (strong, nonatomic) IBOutlet UIButton *bototnOutZoom;
@property (strong, nonatomic) IBOutlet UIView *pruebaVista;

@property (nonatomic) NSInteger selectedButton;
- (IBAction)cambiaColor:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITableView *products;


- (IBAction)mueve:(UIButton *)sender;
- (IBAction)botonesSuperiores:(UIButton *)sender;

- (IBAction)prueba:(id)sender;
- (IBAction)botonZoom:(UIButton *)sender;
- (void)buttonClicked: (id*)sender;
@end
