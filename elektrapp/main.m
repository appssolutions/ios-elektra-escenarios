//
//  main.m
//  elektrapp
//
//  Created by Rafael Castañeda on 08/08/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
