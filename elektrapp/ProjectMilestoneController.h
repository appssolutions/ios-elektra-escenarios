//
//  ProjectMilestoneController.h
//  elektrapp
//
//  Created by Rafael Castañeda on 03/09/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProjectMilestoneController : UITableViewController{


    NSMutableArray *categorias;

}
- (void) muestraTabla;
@end



@interface ProjectMilestoneController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UITableView *tableView;

@end