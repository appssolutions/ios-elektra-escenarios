//
//  ConfiguradorView.m
//  elektrapp
//
//  Created by Rafael Castañeda on 14/08/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import "ConfiguradorView.h"
#import "Productos.h"


@interface ConfiguradorView ()

@end

@implementation ConfiguradorView
@synthesize selectedButton;
@synthesize vistaMenuTop;
@synthesize vistaMenuDown;
@synthesize bototnOutZoom;
@synthesize escenarioVistas;
@synthesize mueveImagen;
@synthesize imagenCentro;
@synthesize imagenDerecha;
@synthesize imagenIzquierda;
@synthesize posiciones;
@synthesize colores;
@synthesize mueveDerecha;
@synthesize mueveFondo;
@synthesize pruebaVista;

@synthesize listaRamas;
@synthesize cargaRamaActual;


NSString * cambiarColor;
int botonPos;
int botonColor;
int cambiaMenu;
Productos *cargaProductos ;

UILabel *dos;
NSMutableArray *recipes;


- (void) giraDerecha
{
    CGRect cargavista = mueveDerecha.frame;
    CGRect mueveVista = CGRectMake(cargavista.origin.x-100, cargavista.origin.y, cargavista.size.width+100, cargavista.size.height+10);
    
    
    CGRect cargafondo = mueveFondo.frame;
    CGRect mueveElFondo = CGRectMake(cargafondo.origin.x-65, cargafondo.origin.y, cargafondo.size.width, cargafondo.size.height-10);
    
    
    [UIView animateWithDuration:0.300 delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         
                         mueveDerecha.frame = mueveVista;
                         mueveFondo.frame = mueveElFondo;
                         
                         NSLog(@"dd");
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
   
    



}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
   [self touchesEnded:touches withEvent:event];
    
    
    UITouch *mover = [[event allTouches] anyObject];
    
    CGPoint localiza = [mover locationInView:escenarioVistas];
    
    
    if ([mover view] == mueveImagen) {
        
        mueveImagen.center = localiza;
    }
    
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{

       [self touchesBegan:touches withEvent:event];
    
    


}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *mover = [[event allTouches] anyObject];
    
    CGPoint localiza = [mover locationInView:escenarioVistas];
    
    if (mueveImagen.center.x > 3030 )  {
        localiza.x = 3030;
       mueveImagen.center = localiza; 
       
    }
    
    ///
    if ( mueveImagen.center.x < 30)  {
        localiza.x = 30;
        mueveImagen.center = localiza;
        
    }
    ///
    
    
    
    if (mueveImagen.center.y > 623 )  {
        localiza.y = 623;
        mueveImagen.center = localiza;
      
    }

    
    
    if ( mueveImagen.center.y < 33)  {
        localiza.y = 33;
       mueveImagen.center = localiza;
       
    }

    
    
   
  

}


/*- (void) pinch:(UIPinchGestureRecognizer *) recognizer {
    
    CGPoint anchor = [recognizer locationInView:mueveImagen];
    anchor = CGPointMake(anchor.x - mueveImagen.bounds.size.width/2, anchor.y-mueveImagen.bounds.size.height/2);
    
    CGAffineTransform affineMatrix = mueveImagen.transform;
    affineMatrix = CGAffineTransformTranslate(affineMatrix, anchor.x, anchor.y);
    affineMatrix = CGAffineTransformScale(affineMatrix, [recognizer scale], [recognizer scale]);
    affineMatrix = CGAffineTransformTranslate(affineMatrix, -anchor.x, -anchor.y);
    mueveImagen.transform = affineMatrix;
    
    [recognizer setScale:1];
}
*/





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self giraDerecha];
    listaRamas = [[ProductosDAO alloc]init];
    cargaRamaActual = [[NSMutableArray alloc]init];
    
    cargaRamaActual = [listaRamas obtenerArbolProductos];
    
    
    
    cambiaMenu = 0;
    
   // recipes = [NSArray arrayWithObjects:@"Linea Blanca", @"Electrodomesticos", @"Muebles", @"Electrònica", nil];
    
  recipes = [[NSMutableArray alloc]init];
    
    
    for (int xc = 0; xc<5; xc++) {
        cargaProductos = [[Productos alloc]init];
        cargaProductos.idProducto = xc;
        //cargaProductos.describeProducto = @"prueba";
        [recipes addObject:cargaProductos];
    }
    
   
    
   
   

    
    
        
    //UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
   // [self.view addGestureRecognizer:pinchGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonClicked :(id*)sender{

    dos.text = @"sdfsdfsdfds";
    
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame = CGRectMake(20, 20, 400, 400); // position in the parent view and set the size of the button
    [myButton setTitle:@"Click Me!" forState:UIControlStateNormal];
    // add targets and actions
    [myButton addTarget:self action:@selector(buttonClicked2:) forControlEvents:UIControlEventTouchUpInside];
    myButton.tag =7;
    
    
    
    //UITableView * aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
	
    
   // [UIView transitionWithView:self.pruebaVista duration:0.8
     //                  options:UIViewAnimationOptionTransitionCrossDissolve
       //             animations:^ { [self.products reloadData];  }
         //           completion:nil];
    
    //[self.products reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    //[self.products reloadData];
    
    recipes = [NSArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Inzzzstant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    

    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self.products
                          duration:0.1f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            [self.products reloadData];
                        } completion:NULL];
    });
    
}



- (void)buttonClicked2: (id*)sender{
    
    for (UIView *subview in [self.pruebaVista subviews]) {
        if (subview.tag == 7) {
            [subview removeFromSuperview];
        }
    }
    
}


- (IBAction)cambiaColor:(UIButton *)sender {
    
    cambiarColor =[NSString stringWithFormat:@"co%d.jpg", [sender tag]];
    [self.imagenCentro setImage: [UIImage imageNamed:cambiarColor]];
    NSLog(@"%d",[sender tag]);
  
    //[self giraDerecha];

    
}

- (IBAction)mueve:(UIButton *)sender {
    int origenx = 0;
    switch ([sender tag]) {
        case 1:
            origenx = 0;
            break;
            
        case 2:
            origenx = -1024;
            break;
            
            
        case 3:
            origenx = -2048;
            break;
            
        default:
            origenx = 0;
            break;
    }

    CGRect cargavista = escenarioVistas.frame;
    CGRect mueveVista = CGRectMake(origenx, cargavista.origin.y, cargavista.size.width, cargavista.size.height);
    
    
    [UIView animateWithDuration:0.300 delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         
                         escenarioVistas.frame = mueveVista;
                        
                         
                     } completion:^(BOOL finished) {
                         
                     }];


}

- (IBAction)botonesSuperiores:(UIButton *)sender {
    
    
    if ([sender tag] == 11) {
        
        if (botonPos == 0) {
            [posiciones setHidden:FALSE];
            [colores setHidden:TRUE];
            botonPos = 1;
        }
        else{
        
        [posiciones setHidden:TRUE];
            botonPos = 0;
        }
    
    }
    
    
    if ([sender tag] == 14) {
        
        
        if (botonColor == 0) {
            [colores setHidden:FALSE];
            [posiciones setHidden:TRUE];
            botonColor = 1;
        }
        else{
            
             [colores setHidden:TRUE];
            botonColor = 0;
        }
        
    }
}



- (IBAction)prueba:(id)sender {
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(handleLongPress:)];
    
   [longPress setDelegate:self];
    longPress.minimumPressDuration = 2.0;
    [self.boton addGestureRecognizer:longPress];
    
}


- (IBAction)botonZoom:(UIButton *)sender {
    
    
   
    CGRect origenOcultaMenuTop = vistaMenuTop.frame;
    CGRect origenOcultaMenuDown = vistaMenuDown.frame;

    CGRect creceEscenario = escenarioVistas.frame;

    
    [posiciones setHidden:TRUE];
    [colores setHidden:TRUE];
    
    //[escenarioVistas setHidden:TRUE];
    
    
    if ([sender tag]==1) {
        
    bototnOutZoom.hidden = false;
    
    
    CGRect zoomOcultaMenuTop = CGRectMake(origenOcultaMenuTop.origin.x,
                                          origenOcultaMenuTop.origin.y,
                                          origenOcultaMenuTop.size.width,
                                          origenOcultaMenuTop.size.height-45);
        
   CGRect zoomOcultaMenuDown = CGRectMake(origenOcultaMenuDown.origin.x,
                                               origenOcultaMenuDown.origin.y+48,
                                               origenOcultaMenuDown.size.width,
                                               origenOcultaMenuDown.size.height);
        
        
        CGRect creciendoescenario = CGRectMake(creceEscenario.origin.x,
                                               creceEscenario.origin.y - 20,
                                               creceEscenario.size.width,
                                               creceEscenario.size.height);
        
        
        
 
        
        
    
    
    [UIView animateWithDuration:0.300 delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         escenarioVistas.frame = creciendoescenario;
                         vistaMenuTop.frame = zoomOcultaMenuTop;
                         vistaMenuTop.clipsToBounds = YES;
                         vistaMenuDown.frame = zoomOcultaMenuDown;
                         vistaMenuDown.clipsToBounds = YES;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
        
        
                
        
        
        
    
    }
    
    else
    {
        CGRect zoomOcultaMenuTop = CGRectMake(origenOcultaMenuTop.origin.x,
                                              origenOcultaMenuTop.origin.y,
                                              origenOcultaMenuTop.size.width,
                                              origenOcultaMenuTop.size.height+45);
        
        CGRect zoomOcultaMenuDown = CGRectMake(origenOcultaMenuDown.origin.x,
                                               origenOcultaMenuDown.origin.y-48,
                                               origenOcultaMenuDown.size.width,
                                               origenOcultaMenuDown.size.height);
        
        
        
        CGRect creciendoescenario = CGRectMake(creceEscenario.origin.x,
                                               creceEscenario.origin.y+20,
                                               creceEscenario.size.width,
                                               creceEscenario.size.height);
        
        
        [UIView animateWithDuration:0.300 delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             escenarioVistas.frame = creciendoescenario;
                             vistaMenuTop.frame = zoomOcultaMenuTop;
                             vistaMenuTop.clipsToBounds = YES;
                             vistaMenuDown.frame = zoomOcultaMenuDown;
                             vistaMenuDown.clipsToBounds = YES;
                             
                         } completion:^(BOOL finished) {
                             
                         }];
        
        
        
        
        


    
    }
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recipes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RecipeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
        
    //cargaProductos = [recipes objectAtIndex:indexPath.row];
    //cell.textLabel.text = cargaProductos.describeProducto;
    cell.textLabel.text = [[cargaRamaActual objectAtIndex:[indexPath row]] valueForKey:@"describeProductos"];
    
   // NSLog(@"%d",cargaProductos.idProducto);
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    
    
    NSLog(@"%ld",(long)indexPath.row);
    
    
    if ((indexPath.row == 0) && (cambiaMenu==0)) {
        cambiaMenu=1;
        
        
        recipes = [NSArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Inzzzstant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    }
    
    
    
    
    if ((indexPath.row == 0) && (cambiaMenu==0)) {
        cambiaMenu=1;
        
        
        recipes = [NSArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Inzzzstant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    }
    
    
        
    [self.products reloadData];
    
}



@end
