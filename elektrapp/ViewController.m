//
//  ViewController.m
//  elektrapp
//
//  Created by Rafael Castañeda on 08/08/13.
//  Copyright (c) 2013 Grupo Sellcom. All rights reserved.
//

#import "ViewController.h"
#import "ConfiguradorView.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize despliegaEscenarios;
@synthesize vistaEscenario;
@synthesize seleccionRecamara;
@synthesize seleccionCocina;
@synthesize seleccionComedor;
@synthesize seleccionSala;
@synthesize scrollview;
@synthesize vista;
@synthesize miSeleccion;
NSInteger activo;
NSInteger pasarela;
UIButton *myButton;
UIImage *buttonImage;




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    activo = 0;
    seleccionRecamara.alpha=0.0;
    seleccionCocina.alpha=0.0;
    seleccionComedor.alpha=0.0;
    seleccionSala.alpha=0.0;
    
    
    


   
    myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myButton.frame = CGRectMake(960, 3, 31, 27);
    
    buttonImage = [UIImage imageNamed:@"e_0000_img_barra_menu.png"];
    [myButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    scrollview.hidden = FALSE;
    
    scrollview.showsHorizontalScrollIndicator = YES;
    scrollview.showsVerticalScrollIndicator = NO;
    [scrollview setCanCancelContentTouches:NO];
    
    //scrollview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"e_0008_Btn_recamara.png"]];
    
    scrollview.backgroundColor = [UIColor colorWithRed:66/255.0f
                                                 green:66/255.0f
                                                  blue:66/255.0f
                                                 alpha:1.0f];
    [self desactivaOpciones];

    
    // add targets and actions
    [myButton addTarget:self action:@selector(invocaEsenarios:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:myButton];
    ///
    
    
   
    
    
    
    
    ///

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





- (IBAction)seleccionEscenario:(UIButton *)sender {
    
   scrollview.hidden = FALSE;
    [self desactivaOpciones];
    
    
    switch (sender.tag) {
        case 30:
            [self desplegandoEscenario:9 :1];
            
            [seleccionRecamara setSelected:YES];
            
            
            
            
            break;
            
        case 31:
            [self desplegandoEscenario:9 :10];
            
            
            
            [seleccionComedor setSelected:YES];

            break;
            
        case 32:
            [self desplegandoEscenario:9 :28];
           
            [seleccionCocina setSelected:YES];
            
            break;
            
        case 33:
            [self desplegandoEscenario:9 :19];
            [seleccionSala setSelected:YES];
            
            break;
            
        default:
            break;
    }

    
}

- (void)desactivaOpciones {
     [seleccionComedor setSelected:NO];
     [seleccionCocina setSelected:NO];
     [seleccionRecamara setSelected:NO];
     [seleccionSala setSelected:NO];



}
- (void)desplegandoEscenario: (int) secciones : (int)comienzaEn{

    int anchoScroll = 0;
    
    NSString * pantallas;
    
    anchoScroll = secciones * 260;
    
    
    scrollview.contentSize=CGSizeMake(anchoScroll, 200);
   

    
    for (UIView *subview in scrollview.subviews) {
        [subview removeFromSuperview];
    
    }
    [scrollview setNeedsDisplay];
    
     [scrollview scrollRectToVisible:CGRectMake(0,43,733,211) animated:NO];
    
    float valeX = 35.0;
    for (NSInteger cantidadB = 1; cantidadB<=secciones; cantidadB++) {
       //NSLog(@"%d",secciones);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button addTarget:self  action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
        
        pantallas = [NSString stringWithFormat:@"%da.jpg",comienzaEn ];
        
      UIImage*  btnImage = [UIImage imageNamed:pantallas];
        [button setImage:btnImage forState:UIControlStateNormal];
        
        
       // NSString *mensaje= [NSString stringWithFormat:@"%d",comienzaEn ];
        //[button setTitle: mensaje forState:UIControlStateNormal];
        [button setTag:comienzaEn];
        button.frame = CGRectMake(valeX, 23.0, 220.0, 164.0);
        [scrollview addSubview:button];
        valeX = valeX + 250.00;
        comienzaEn++;
    }
    
    
      
    
}


- (void)buttonClicked:(UIButton*)button
{
    //NSLog(@"Button %@ clicked.", );
    
  NSString *pantalla = [NSString stringWithFormat:@"%d.jpg",[button tag] ];
    
    UIImage*  btnImage = [UIImage imageNamed:pantalla];
    [miSeleccion setImage:btnImage forState:UIControlStateNormal];

    
    
    vista.text = [NSString stringWithFormat:@"%d", [button tag] ];
    pasarela = [button tag];
    
    //[miSeleccion setTitle: [NSString stringWithFormat:@"%d", [button tag] ] forState: UIControlStateNormal];
}

- (IBAction)invocaEsenarios:(id)sender {
   
    
           

    
    
    
    if (activo == 0) {
        
       buttonImage = [UIImage imageNamed:@"e_0001_img_barra_menu_press.png"];
        [myButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self desactivaOpciones];

        

        
        activo = 1;
        seleccionRecamara.alpha=1.0;
        seleccionCocina.alpha=1.0;
        seleccionComedor.alpha=1.0;
        seleccionSala.alpha=1.0;
           
        
    CGRect uiview2_original_rect = despliegaEscenarios.frame;
    
    
   
    
    CGRect uiview2_resized_rect = CGRectMake(uiview2_original_rect.origin.x,
                                             uiview2_original_rect.origin.y,
                                             uiview2_original_rect.size.width,
                                             uiview2_original_rect.size.height+211);
    
    [UIView animateWithDuration:0.300 delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         
                         despliegaEscenarios.frame = uiview2_resized_rect;
                         despliegaEscenarios.clipsToBounds = YES;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    }
    
    else {
        buttonImage = [UIImage imageNamed:@"e_0000_img_barra_menu.png"];
        [myButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        activo = 0;
    scrollview.hidden = TRUE;
        CGRect uiview2_original_rect = despliegaEscenarios.frame;
        
        
        
        
        CGRect uiview2_resized_rect = CGRectMake(uiview2_original_rect.origin.x,
                                                 uiview2_original_rect.origin.y,
                                                 uiview2_original_rect.size.width,
                                                 uiview2_original_rect.size.height-211);
        
        [UIView animateWithDuration:0.300 delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             despliegaEscenarios.frame = uiview2_resized_rect;
                             despliegaEscenarios.clipsToBounds = YES;
                             
                         } completion:^(BOOL finished) {
                             
                         }];
    
        
        scrollview.backgroundColor = [UIColor colorWithRed:255/255.0f
                                                     green:255/255.0f
                                                      blue:255/255.0f
                                                     alpha:1.0f];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"salaConfigurador"]) {
        
        // Get destination view
        ConfiguradorView *vc = [segue destinationViewController];
        
        // Get button tag
       // NSInteger tagIndex = [(UIButton *)sender tag];
        
        // Set the selected button in the new view
        [vc setSelectedButton: pasarela];
    }
}


@end
